package com.atlassian.sourcemap;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestSourceMapWithNames {

    private Gson gson = new Gson();

    @Test public void shouldParseSourceMap() throws IOException {
        String mapAsString = Resources.toString(Resources.getResource("test.map"), Charsets.UTF_8);

        final SourceMap check = new SourceMapImpl();
        final SourceMap original = new SourceMapImpl(mapAsString);
        original.eachMapping(check::addMapping);

        // parse json and convert to string again to ignore formatting differences
        assertThat(gson.toJson(gson.fromJson(check.generate(), Consumer.SourceMapJson.class)), equalTo(gson.toJson(gson.fromJson(mapAsString, Consumer.SourceMapJson.class))));
    }
}
